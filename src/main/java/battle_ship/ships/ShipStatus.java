package battle_ship.ships;

public enum ShipStatus {
    OK, HIT, DESTROYED;
}
