package battle_ship.ships;

import battle_ship.boards.Board;
import battle_ship.boards.FieldStatus;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ship {
    private static final Logger LOGGER = Logger.getLogger(Ship.class.getName());
    private int length;
    private ShipStatus status;
    private String startField;
    private String endField;
    ConsoleHandler handler = new ConsoleHandler();
    SimpleFormatter formatter = new SimpleFormatter();

    public void setStatus(ShipStatus status) {
        this.status = status;
    }

    public Map<String, FieldStatus> partStatuses;

    public Ship(int length, String startField, String endField) {
        LOGGER.addHandler(handler);
        handler.setFormatter(formatter);
        handler.setLevel(Level.OFF);
        this.length = length;
        this.status = ShipStatus.OK;
        this.startField = startField;
        this.endField = endField;
        this.partStatuses = new LinkedHashMap<>();
        if (Board.parseRow(this.startField) == Board.parseRow(this.endField)) {
            for (int i = Board.parseColumn(this.startField); i <= Board.parseColumn(this.endField); i++) {
                partStatuses.put(String.valueOf(startField.charAt(0)) + (i + 1), FieldStatus.FULL);
            }
        } else {
            for (int i = Board.parseRow(this.startField); i <= Board.parseRow(this.endField); i++) {
                partStatuses.put(Board.rowToString(i) + (Board.parseColumn(this.startField) + 1), FieldStatus.FULL);
            }
        }

    }

    public static boolean checkSyntax(String shipDefinition) {
        Pattern pattern = Pattern.compile("[A-J][1-9][0]?:[A-J][1-9][0]?");
        Matcher matcher = pattern.matcher(shipDefinition);
        boolean matches = matcher.matches();
        if (!matches) {
            LOGGER.log(Level.FINE, "regex mismatch");
            return false;
        }
        StringTokenizer st = new StringTokenizer(shipDefinition, ":");
        String start = st.nextToken();
        String end = st.nextToken();
        if (start.charAt(0) > end.charAt(0)) {
            LOGGER.log(Level.FINE,"vertical wrong order");
            return false;
        }
        if (Integer.parseInt(start.substring(1)) > Integer.parseInt(end.substring(1))) {
            LOGGER.log(Level.FINE,"horizontal wrong order");
            return false;
        }
        if (start.charAt(0) != end.charAt(0) && Integer.parseInt(start.substring(1)) != Integer.parseInt(end.substring(1))) {
            LOGGER.log(Level.FINE,"diagonal");
            return false;
        }
        if (Integer.parseInt(end.substring(1)) - Integer.parseInt(start.substring(1)) > 3) {
            LOGGER.log(Level.FINE,"length");
            return false;
        }
        if (end.charAt(0) - start.charAt(0) > 3) {
            LOGGER.log(Level.FINE,"length");
            return false;
        }
        return true;
    }

    public void evaluateStatus() {
        for (Map.Entry<String, FieldStatus> entry : partStatuses.entrySet()) {
            if (entry.getValue() == FieldStatus.HIT) {
                this.status = ShipStatus.HIT;
            }
            if (entry.getValue() != FieldStatus.HIT) {
                return;
            }
        }
        this.status = ShipStatus.DESTROYED;
    }

    public int getLength() {
        return length;
    }

    public ShipStatus getStatus() {
        return status;
    }

    public String getStartField() {
        return startField;
    }

    public String getEndField() {
        return endField;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "length=" + length +
                ", status=" + status +
                ", startField='" + startField + '\'' +
                ", endField='" + endField + '\'' +
                ", handler=" + handler +
                ", formatter=" + formatter +
                ", partStatuses=" + partStatuses +
                '}';
    }


}
