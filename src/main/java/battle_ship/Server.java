package battle_ship;

import battle_ship.json.JHandler;
import battle_ship.players.Player;
import battle_ship.players.ServerPlayer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class Server {

    static Vector<Player> ar = new Vector<>();
    static int i = 0;

    public static void main(String[] args) throws IOException {


        AtomicBoolean running = new AtomicBoolean(true);

        

        ServerSocket ss = new ServerSocket(1234);
        Scanner scanner = new Scanner(System.in);

while (true) {
    try {
        System.out.println(JHandler.getMessageType("{\"type\":\"SHOT_REQUEST\",\"status\":0,\"message\":null,\"body\":{\"row\":\"A\",\"column\":5}}"));
        Socket s = null;
        s = ss.accept();

        System.out.println("New client request received : " + s);

        DataInputStream inputStream = new DataInputStream(s.getInputStream());
        DataOutputStream outputStream = new DataOutputStream(s.getOutputStream());

        System.out.println("Creating a new handler for this client...");

        System.out.println("remoteSocketAddr: " + s.getRemoteSocketAddress().toString());

        //outputStream.writeUTF("login");
        ServerPlayer serverPlayer = new ServerPlayer("client " + i, s, inputStream, outputStream);

        Thread t = new Thread(serverPlayer);

        System.out.println("Adding this client to active client list");
        ar.add(serverPlayer);
        t.start();

        i++;
    } catch (NullPointerException nullPointerException) {
        nullPointerException.printStackTrace();
        running.set(false);

    } catch (Exception e) {
        e.printStackTrace();
        running.set(false);

    }


}


    }

}
