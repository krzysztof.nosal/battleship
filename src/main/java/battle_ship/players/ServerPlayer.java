package battle_ship.players;

import battle_ship.boards.Board;
import battle_ship.boards.FieldStatus;
import battle_ship.json.*;
import battle_ship.ships.Ship;
import battle_ship.ships.ShipStatus;
import com.google.gson.JsonSyntaxException;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;

public class ServerPlayer extends Player implements Runnable {
    private final DataOutputStream outputStream;
    private final BufferedReader bufferedReader;
    private final Socket s;
    private Path currentRelativePath;
    private String logPath;
    private String lastShot = "";
    FileWriter myWriter =null;
    BufferedWriter bw =null;

    public ServerPlayer(String name, Socket s,
                        DataInputStream inputStream, DataOutputStream outputStream) {
        super(name);
        this.outputStream = outputStream;
        this.s = s;
        this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        this.currentRelativePath = Paths.get("");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        this.logPath=currentRelativePath.toAbsolutePath().toString() + "\\src\\main\\resources\\log"+"_"+dtf.format(now)+".txt";
    }

    private void invitationHandling() {
        jsonStatus jsonStatus = null;
        String msg = "";
        while (jsonStatus == null) {
            System.out.println("testttt");
            try {
                msg = bufferedReader.readLine();
                System.out.println(msg);
            }catch (Exception e){e.printStackTrace();}

            try {

                myWriter = new FileWriter(logPath, true);
                myWriter.append("Received message: \n"+msg+"\n");
                myWriter.close();
                try {
                    jsonStatus = JsonHandler.getStatusFromJson(msg);
//                    jsonCommand jsonCommand1 = new jsonCommand("status");
//                    jsonCommand1.setStatus(1);
                    if (jsonStatus.type.equals("GAME_INVITATION")) {
                        myWriter = new FileWriter(logPath, true);
                        myWriter.append("sent response: \n"+"{\"type\":\"GAME_INVITATION\",\"status\":0,\"message\":null,\"body\":null}\n");
                        myWriter.close();
                        outputStream.writeBytes("{\"type\":\"GAME_INVITATION\",\"status\":0,\"message\":null,\"body\":null}\n");
                        break;
                    } else {
                        myWriter = new FileWriter(logPath, true);
                        myWriter.append("sent response: \n"+"{\"type\":\"GAME_INVITATION\",\"status\":1,\"message\":\"wrong command or playing\"}\n");
                        myWriter.close();
                        outputStream.writeBytes("{\"type\":\"GAME_INVITATION\",\"status\":1,\"message\":\"wrong command or playing\"}\n");
                    }
                } catch (JsonSyntaxException e) {
                    myWriter = new FileWriter(logPath, true);
                    myWriter.append("sent response: \n"+"{\"type\":\"GAME_INVITATION\",\"status\":1,\"message\":\"wrong command or playing\"}\n");
                    myWriter.close();
                    outputStream.writeBytes("{\"type\":\"GAME_INVITATION\",\"status\":1,\"message\":\"wrong command or playing\"}\n");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean answeringForShoots() throws IOException {
        JShoot jShoot = null;
        String msg = "";
        JShootResult jShootResult = new JShootResult("HIT");
        while (jShootResult.getBody().equals("HIT") || jShootResult.getBody().equals("SINKING")) {

            msg = this.bufferedReader.readLine();
            myWriter = new FileWriter(logPath, true);
            myWriter.append("<<<<<<<<<<<<<<<<<\n");
            myWriter.append("received message: "+msg+"\n");
            myWriter.close();
            try {
                jShoot = JHandler.shotConvert(msg);
                if (jShoot.getType().equals("SHOT")) {
                    jShootResult = new JShootResult(getMyBoard().shootAtField(jShoot.getFieldString()).toString());
                    outputStream.writeBytes(JHandler.shotResultConvert(jShootResult) + "\n");
                    String shot = "Opponnent is shootnig at: " + jShoot.getFieldString() + " result: " + jShootResult.getBody();
                    myWriter = new FileWriter(logPath, true);
                    myWriter.append(shot+"\n");
                    myWriter.append(printBothBoards());
                    myWriter.append("My response: "+JHandler.shotResultConvert(jShootResult) + "\n\n");
                    myWriter.close();
                    LOGGER.log(Level.INFO, shot);
                    String bothboards = printBothBoards();
                    LOGGER.log(Level.INFO, bothboards);
                    if (getMyBoard().checkWinConditions()) {
                        return true;

                    }

                } else {
                    outputStream.writeBytes("{\"type\":\"SHOT\",\"status\":1,\"message\":\"command " + jShoot.getType() + " not allowed in this part of the game. you should shoot\"}\n");
                }
            } catch (Exception e) {
                outputStream.writeBytes("{\"type\":\"error\",\"status\":1,\"message\":\"wrong json syntax or not allowed command\"}\n");
            }

        }
        return false;

    }

    private boolean shootingAtClient() throws IOException {
        JResult jResult = new JResult("HIT");
        JShotRequest jShotRequest = null;
        JShotRequestResult jShotRequestResult = null;
        boolean iWon = false;
        String msg="";
        String msg2="";
        while (jResult != null && (jResult.getBody().equals("HIT") || jResult.getBody().equals("SINKING"))) {
            if (iWon) {
                myWriter = new FileWriter(logPath, true);
                myWriter.append("confirming reception of shot's result: \n"+"{\"type\":\"RESULT\",\"status\":0,\"message\":null,\"body\":null}\n");
               myWriter.close();
                outputStream.writeBytes("{\"type\":\"RESULT\",\"status\":0,\"message\":null,\"body\":null}\n");
                LOGGER.log(Level.INFO, "I WON!!!!!!");
                return false;
            }
            try {
                msg2=bufferedReader.readLine();
                jShotRequest = JHandler.shotRequestConvert(msg2);
                if (jShotRequest.getType().equals("SHOT_REQUEST")) {
                    String randShot;
                    if (lastShot.equals("")) {
                        randShot = randomShootAtOpponent();
                    } else {
                        randShot = randomContinueShooting(lastShot);
                    }
                    jShotRequestResult = new JShotRequestResult(randShot);
                    outputStream.writeBytes(JHandler.shotRequestResultConvert(jShotRequestResult) + "\n");
                    jResult = null;
                    while (jResult == null) {
                        try {
                            msg=bufferedReader.readLine();
                            jResult = JHandler.resultConvert(msg);
                        } catch (Exception e) {
                            outputStream.writeBytes("{\"type\":\"error\",\"status\":1,\"message\":\"wrong json syntax or not allowed command\"}\n");
                        }
                    }
                    switch (jResult.getBody()) {
                        case "HIT":
                            opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.HIT);
                            lastShot = randShot;
                            disableInpossibleLocations(lastShot);
                            break;
                        case "MISS":
                            opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.MISS);
                            break;
                        case "SINKING":
                            opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.HIT);
                            lastShot = randShot;
                            disableInpossibleLocations(lastShot);
                            Ship ship = opponentBoard.getShipFromBoard(lastShot);
//                            myWriter = new FileWriter(logPath, true);
//                            myWriter.append("ship sinken: "+ship.toString()+"\n");
//                            myWriter.close();
                            disableImpossibleLocationsAfterSinking(ship);
                            ship.setStatus(ShipStatus.DESTROYED);
                            opponentBoard.shipList.add(ship);
                            if (opponentBoard.checkWinConditions()) {
                                iWon = true;
                                break;
                            }
                            lastShot = "";
                            break;
                        default:
                            break;

                    }
                    outputStream.writeBytes("{\"type\":\"RESULT\",\"status\":0,\"message\":null,\"body\":null}\n");
                    myWriter = new FileWriter(logPath, true);
                    myWriter.append(">>>>>>>>>>>>>>>>>>>>>\n");
                    myWriter.append("received shot request: "+msg2+"\n");
                    myWriter.append("I'm shooting at client: "+randShot+" result: "+jResult.getBody().toString()+"\n");
                    myWriter.append("My message: "+JHandler.shotRequestResultConvert(jShotRequestResult) + "\n");
                    myWriter.append("result: "+msg+"\n");
                    myWriter.append("confirming reception of shot's result: \n"+"{\"type\":\"RESULT\",\"status\":0,\"message\":null,\"body\":null}\n");
                    myWriter.append(printBothBoards()+"\n");
                    myWriter.close();
                    System.out.println("last shot:" + randShot);
                    System.out.println(printBothBoards());

                } else
                    outputStream.writeBytes("{\"type\":\"SHOT\",\"status\":1,\"message\":\"command " + jShotRequest.getType() + " not allowed in this part of the game. you should request for my shoot\"}\n");


            } catch (Exception e) {
                outputStream.writeBytes("{\"type\":\"error\",\"status\":1,\"message\":\"wrong json syntax or not allowed command\"}\n");
            }
        }
        return true;
    }

    @Override
    public void run() {
        try{
            System.out.println(logPath);
            File myFile=new File(logPath);
            myFile.createNewFile();
            while (!myFile.exists()){
                System.out.println("waiting");
            }
            myWriter = new FileWriter(logPath, true);
            myWriter.append("client has connected\n");
            myWriter.close();
        }catch (Exception e){e.printStackTrace();}
        int step = 0;
        try {

            while (step < 999) {
                switch (step) {
                    case 0:
                        invitationHandling();
                        myWriter = new FileWriter(logPath, true);
                        myWriter.append("Ip of client: "+s.getRemoteSocketAddress().toString()+"\n");
                        myWriter.close();
                        step++;
                        break;
                    case 1:
                        step++;
                        break;

                    case 2:
                        System.out.println();
                        randomAddShips();
                        myWriter = new FileWriter(logPath, true);
                        myWriter.append("randomly added ships\n");
                        myWriter.append(printBothBoards()+"\n");
                        myWriter.close();
                        step++;
                        break;
                    case 3:
                        System.out.println("case 3");
                        if (answeringForShoots()) {
                            LOGGER.log(Level.INFO, "I Lost");
                            myWriter = new FileWriter(logPath, true);
                            myWriter.append("I LOST \n");
                            myWriter.close();
                            step = 5;
                        } else {
                            step++;
                        }
                        break;
                    case 4:
                        System.out.println("start shooting last shot: "+lastShot);
                        if (shootingAtClient()) {
                            step = 3;
                        } else {
                            myWriter = new FileWriter(logPath, true);
                            myWriter.append("I WON \n");
                            myWriter.close();
                            step = 5;
                        }
                        break;
                    case 5:
                        String ttttt = bufferedReader.readLine();
                        JBoardSummary jBoardSummary = JHandler.boardSummaryConvert(ttttt);
                        myWriter = new FileWriter(logPath, true);
                        myWriter.append("Board summary from client \n");
                        myWriter.append(jBoardSummary+"\n");
                        myWriter.append("raw answer: "+ttttt);
                        outputStream.writeUTF("{\"type\":\"BOARD\",\"status\":0,\"message\":null,\"body\":null}\n");
                        myWriter.append("confirmation: "+"{\"type\":\"BOARD\",\"status\":0,\"message\":null,\"body\":null}\n");
                        myWriter.close();
                        System.out.println(jBoardSummary);
                        System.out.println("quit");
                        s.close();
                        step = 1000;
                        break;
                    case 6:
                        System.out.println("case 6");
                        String test2 = bufferedReader.readLine();
                        System.out.println(test2);
                        JBoardSummary jBoardSummary2 = JHandler.boardSummaryConvert(bufferedReader.readLine());
                        System.out.println(jBoardSummary2);
                        System.out.println("quit");
                        step = 1000;
                        s.close();
                        break;

                    default:

                        break;
                }
            }
        } catch (Exception e) {
        }
//test
    }
}
