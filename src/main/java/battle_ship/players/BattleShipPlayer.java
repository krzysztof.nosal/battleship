package battle_ship.players;

public interface BattleShipPlayer {
    public PlayerActionResult shoot(String field);
    public PlayerActionResult invite();
    public PlayerActionResult shootRequest();

}
