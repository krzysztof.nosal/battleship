package battle_ship.players;

public enum PlayerActionResult {
    APPROVE, DECLINE, HIT, MISS,SINKING, ERROR;
}

