package battle_ship.players;

import battle_ship.boards.*;
import battle_ship.ships.*;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import java.util.logging.SimpleFormatter;


public class Player implements BattleShipPlayer {
    private Board myBoard;
    protected Board opponentBoard;
    protected String name;
    protected static final Logger LOGGER = Logger.getLogger(Player.class.getName());
    ConsoleHandler handler = new ConsoleHandler();
    SimpleFormatter formatter = new SimpleFormatter();
    Random rand = new Random();
    private Board testboard=new Board();

    public Player() {
    }

    public Player(String name) {
        LOGGER.addHandler(handler);
        handler.setFormatter(formatter);
        handler.setLevel(Level.OFF);
        this.name = name;
        this.myBoard = new Board();
        this.opponentBoard = new Board();
    }


    public PlayerActionResult addShips(Scanner scanner) {
        //Scanner scanner = new Scanner(System.in);
        for (int j = 1; j < 5; j++) {
            for (int i = 0; i < (5 - j); i++) {
                String coordinates="init";
                Ship shipToAdd = new Ship(2, "B10","A1" );
                while (!Ship.checkSyntax(coordinates)){

                System.out.println("enter " + j + " mast coordinates:");
                coordinates = scanner.nextLine();
                    if(!Ship.checkSyntax(coordinates)){continue;}
                    StringTokenizer st = new StringTokenizer(coordinates, ":");
                    String start = st.nextToken();
                    String end = st.nextToken();
                    shipToAdd = new Ship(j, start, end);
                    if (!myBoard.canAddShip(shipToAdd)) {
                        coordinates="init";
                        continue;
                        //this.myBoard = new Board();
                        //return PlayerActionResult.ERROR;
                    }
                    testboard=new Board();
                    testboard.addShip(shipToAdd);
                    if (testboard.getShipFromBoard(start).getLength() != j) {
                        coordinates="init";
                        continue;
                        //this.myBoard = new Board();
                        //return PlayerActionResult.ERROR;
                    }
                    myBoard.addShip(shipToAdd);
                    System.out.println(myBoard.toString());

                }
            }

        }

        return PlayerActionResult.APPROVE;
    }

    private boolean addRandomShipsOfOneType(int mastNumber, int count) {
        int tries = 0;
        if (mastNumber == 1) {
            for (int i = 0; i < count; i++) {
                int row = rand.nextInt(10);
                int column;
                if (row == 0 || row == 9) {
                    column = rand.nextInt(10) + 1;
                } else {
                    if (rand.nextInt(10) > 5) {
                        column = 10;
                    } else {
                        column = 1;
                    }
                }

                while (!myBoard.canAddShip(new Ship(1, Board.rowToString(row) + (column), Board.rowToString(row) + (column)))) {
                    tries++;
                    if (tries > 20) {
                        return false;
                    }
                    row = rand.nextInt(10);
                    if (row == 0 || row == 9) {
                        column = rand.nextInt(10) + 1;
                    } else {
                        if (rand.nextInt(10) > 5) {
                            column = 10;
                        } else {
                            column = 1;
                        }
                    }
                    String fieldString = Board.rowToString(row) + (column);
                    LOGGER.log(Level.INFO, fieldString);
                    myBoard.addShip(new Ship(1, fieldString, fieldString));

                }
            }
            return true;
        } else {
            for (int j = 0; j < count; j++) {

                int row = rand.nextInt(9);
                int column = rand.nextInt(9);
                int dir = rand.nextInt(2);
                String start = Board.rowToString(row) + (column + 1);
                String stop;
                if (dir > 0) {
                    row += mastNumber;
                } else {
                    column += mastNumber;
                }
                stop = Board.rowToString(row) + (column + 1);
                while (!myBoard.canAddShip(new Ship(2, start, stop))) {
                    tries++;
                    if (tries > 20) {
                        return false;
                    }
                    row = rand.nextInt(9);
                    column = rand.nextInt(9);
                    dir = rand.nextInt(2);
                    start = Board.rowToString(row) + (column + 1);

                    if (dir > 0) {
                        row += mastNumber;
                    } else {
                        column += mastNumber;
                    }
                    stop = Board.rowToString(row) + (column + 1);
                }
                LOGGER.log(Level.INFO, start);
                LOGGER.log(Level.INFO, stop);
                myBoard.addShip(new Ship(2, start, stop));
                String boardView = myBoard.toString();
                LOGGER.log(Level.INFO, boardView);
            }
            return true;
        }

    }


    public void randomAddShips() {
        int j = 0;
        for (int i = 0; i < 4; i++) {
            int row = rand.nextInt(10);
            int column;
            if (row == 0 || row == 9) {
                column = rand.nextInt(10) + 1;
            } else {
                if (rand.nextInt(10) > 5) {
                    column = 10;
                } else {
                    column = 1;
                }
            }

            while (!myBoard.canAddShip(new Ship(1, Board.rowToString(row) + (column), Board.rowToString(row) + (column)))) {
                row = rand.nextInt(10);
                if (row == 0 || row == 9) {
                    column = rand.nextInt(10) + 1;
                } else {
                    if (rand.nextInt(10) > 5) {
                        column = 10;
                    } else {
                        column = 1;
                    }
                }
            }
            myBoard.addShip(new Ship(1, Board.rowToString(row) + (column), Board.rowToString(row) + (column)));
            String boardView = myBoard.toString();
            LOGGER.log(Level.INFO, boardView);

        }

        for (int i = 0; i < 3; i++) {

            int row = rand.nextInt(9);
            int column = rand.nextInt(9);
            int dir = rand.nextInt(2);
            String start = Board.rowToString(row) + (column + 1);
            String stop;
            if (dir > 0) {
                row++;
            } else {
                column++;
            }
            stop = Board.rowToString(row) + (column + 1);
            while (!myBoard.canAddShip(new Ship(2, start, stop))) {
                row = rand.nextInt(9);
                column = rand.nextInt(9);
                dir = rand.nextInt(2);
                start = Board.rowToString(row) + (column + 1);

                if (dir > 0) {
                    row++;
                } else {
                    column++;
                }
                stop = Board.rowToString(row) + (column + 1);
            }
            myBoard.addShip(new Ship(2, start, stop));
            String boardView = myBoard.toString();
            LOGGER.log(Level.INFO, boardView);
        }

        for (int i = 0; i < 2; i++) {

            int row = rand.nextInt(8);
            int column = rand.nextInt(8);
            int dir = rand.nextInt(2);

            String start = Board.rowToString(row) + (column + 1);
            String stop;
            if (dir > 0) {
                row += 2;
            } else {
                column += 2;
            }
            stop = Board.rowToString(row) + (column + 1);
            while (!myBoard.canAddShip(new Ship(3, start, stop))) {
                row = rand.nextInt(8);
                column = rand.nextInt(8);
                dir = rand.nextInt(2);
                start = Board.rowToString(row) + (column + 1);

                if (dir > 0) {
                    row += 2;
                } else {
                    column += 2;
                }
                stop = Board.rowToString(row) + (column + 1);

            }
            myBoard.addShip(new Ship(3, start, stop));
            String boardView = myBoard.toString();
            LOGGER.log(Level.INFO, boardView);
        }

        int row = rand.nextInt(7);
        int column = rand.nextInt(7);
        int dir = rand.nextInt(2);
        String start = Board.rowToString(row) + (column + 1);
        String stop;
        if (dir > 0) {
            row += 3;
        } else {
            column += 3;
        }
        stop = Board.rowToString(row) + (column + 1);
        while (!myBoard.canAddShip(new Ship(4, start, stop))) {
            j++;
            if (j > 50) {
                myBoard = new Board();
                randomAddShips();
                break;
            }
            row = rand.nextInt(7);
            column = rand.nextInt(7);
            dir = rand.nextInt(2);
            start = Board.rowToString(row) + (column + 1);

            if (dir > 0) {
                row += 3;
            } else {
                column += 3;
            }
            stop = Board.rowToString(row) + (column + 1);
        }
        if (j < 50) {
            myBoard.addShip(new Ship(4, start, stop));
            String boardView = myBoard.toString();
            LOGGER.log(Level.INFO, boardView);
        }

    }

    public String randomShootAtOpponent() {
        int row = rand.nextInt(10);
        int column = rand.nextInt(10);
        while (!opponentBoard.boardFields[row][column].getStatus().equals(FieldStatus.EMPTY)) {
            row = rand.nextInt(10);
            column = rand.nextInt(10);
        }
        //BoardField field =
        return Board.rowToString(row) + (column + 1);
    }

    public String randomContinueShooting(String lastShot) {
        List<String> possibleFields = new LinkedList<>();
        int row = Board.parseRow(lastShot);
        int column = Board.parseColumn(lastShot);
        if (row > 0 && opponentBoard.boardFields[row - 1][column].getStatus().equals(FieldStatus.EMPTY)) {
            possibleFields.add(Board.rowToString(row - 1) + (column + 1));
        }
        if (row < 9 && opponentBoard.boardFields[row + 1][column].getStatus().equals(FieldStatus.EMPTY)) {
            possibleFields.add(Board.rowToString(row + 1) + (column + 1));
        }
        if (column > 0 && opponentBoard.boardFields[row][column - 1].getStatus().equals(FieldStatus.EMPTY)) {
            possibleFields.add(Board.rowToString(Board.parseRow(lastShot)) + (Board.parseColumn(lastShot)));
        }
        if (column < 9 && opponentBoard.boardFields[row][column + 1].getStatus().equals(FieldStatus.EMPTY)) {
            possibleFields.add(Board.rowToString(Board.parseRow(lastShot)) + (Board.parseColumn(lastShot) + 2));
        }
        if (!possibleFields.isEmpty()) {
            int choice = new Random().nextInt(possibleFields.size());
            return possibleFields.get(choice);
        } else {
            return randomShootAtOpponent();
        }
    }

    public void disableInpossibleLocations(String lastHit) {
        int row = Board.parseRow(lastHit);
        int column = Board.parseColumn(lastHit);
        if (row > 0) {
            if (column > 0) {
                opponentBoard.boardFields[row - 1][column - 1].setStatus(FieldStatus.EXCLUDED);
            }
            if (column < 9) {
                opponentBoard.boardFields[row - 1][column + 1].setStatus(FieldStatus.EXCLUDED);
            }
        }
        if (row < 9) {
            if (column > 0) {
                opponentBoard.boardFields[row + 1][column - 1].setStatus(FieldStatus.EXCLUDED);
            }
            if (column < 9) {
                opponentBoard.boardFields[row + 1][column + 1].setStatus(FieldStatus.EXCLUDED);
            }
        }
    }

    public void disableFieldsAroundHit(String lastHit) {
       // System.out.println("diable arount hit: "+lastHit);
        int row = Board.parseRow(lastHit);
        int column = Board.parseColumn(lastHit);
      //  System.out.println("field definition: "+row+" "+column);
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = column - 1; j <= column + 1; j++) {
             //   System.out.println("checking field: "+i +" "+j);
                if (i >= 0 && j >= 0 && i <= 9 && j <= 9) {
//                    System.out.println("correct field definition: "+i+" "+j);
//                    System.out.println("field name: "+Board.rowToString(i)+(j+1));
//                    System.out.println("field status: "+opponentBoard.boardFields[i][j].getStatus());
                    if (opponentBoard.boardFields[i][j].getStatus().equals(FieldStatus.EMPTY) ) {
                       // System.out.println("excluding: "+Board.rowToString(i)+(j+1));
                        opponentBoard.boardFields[i][j].setStatus(FieldStatus.EXCLUDED);
                    }
                }

            }
        }

    }

    public void disableImpossibleLocationsAfterSinking(Ship ship) {
        disableFieldsAroundHit(ship.getStartField());
        disableFieldsAroundHit(ship.getEndField());
    }

    public String printBothBoards() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("////my board///////////////////oponent's board///\n");
        stringBuilder.append("|~ 1 2 3 4 5 6 7 8 9 10||||||~ 1 2 3 4 5 6 7 8 9 10|\n");
        for (int i = 0; i < 10; i++) {
            stringBuilder.append("|").append(Board.rowToString(i)).append(" ");
            for (int j = 0; j < 10; j++) {
                stringBuilder.append(Board.fieldStatusShortcut(myBoard.boardFields[i][j].getStatus())).append(" ");
            }
            stringBuilder.append("||||||").append(Board.rowToString(i)).append(" ");
            for (int k = 0; k < 10; k++) {
                stringBuilder.append(Board.fieldStatusShortcut(opponentBoard.boardFields[i][k].getStatus())).append(" ");

            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public Board getMyBoard() {
        return myBoard;
    }

    @Override
    public PlayerActionResult shoot(String field) {
        return myBoard.shootAtField(field);
    }

    @Override
    public PlayerActionResult invite() {
        return null;
    }

    @Override
    public PlayerActionResult shootRequest() {
        return null;
    }
}
