package battle_ship.clients;

import battle_ship.boards.Board;
import battle_ship.boards.FieldStatus;
import battle_ship.json.*;
import battle_ship.players.Player;
import battle_ship.players.PlayerActionResult;
import battle_ship.ships.Ship;
import battle_ship.ships.ShipStatus;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;

public class SimpleClient extends Player implements Runnable {
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;
    private Scanner scanner = null;
    private Board myBoard;
    private final BufferedReader bufferedReader;

    public SimpleClient(String name,DataInputStream inputStream, DataOutputStream outputStream, Scanner scanner) {
        super(name);
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.scanner = scanner;
        this.myBoard = new Board();
        this.bufferedReader = new BufferedReader(new InputStreamReader(this.inputStream));


    }

    @Override
    public void run() {

        int step = 0;
        try {
            while (step != 999) {
                String msg = "";
                String response = "";
                switch (step) {
                    case 0:
                        System.out.println("type \"invite\" to start a game");
                        while (!msg.equals("invite")) {
                            msg = scanner.nextLine();
                        }
                        //System.out.println(JsonHandler.createJsonCommand(new jsonCommand("GAME_INVITATION")));
                        outputStream.writeBytes("{\"type\":\"GAME_INVITATION\",\"body\":null}\n");

                        step++;
                        break;
                    case 1:
                        response = bufferedReader.readLine();
                        System.out.println(response);
                        jsonStatus jsonStatus = JsonHandler.getStatusFromJson(response);
                        //randomAddShips();
                        //LOGGER.log(Level.INFO,getMyBoard().toString());
                        if (jsonStatus.type.equals("GAME_INVITATION") && jsonStatus.status == 0) {
                            System.out.println("invitation accepted");

                            step=99;
                        }

                        break;
                    case 99:
                        if(addShips(scanner).equals(PlayerActionResult.APPROVE)){;
                        System.out.println(myBoard.toString());
                        step=2;
                        break;}
                    case 2:
                        boolean iWon = false;
                        JShootResult jShootResult = new JShootResult("HIT");
                        String randShot = "";
                        String lastShot = "";
                        while (jShootResult.getBody().equals("HIT") || jShootResult.getBody().equals("SINKING")) {
                            System.out.println(printBothBoards());
                            System.out.println("shoooooot:");
                            randShot=scanner.nextLine();
//                            if (lastShot.equals("")) {
//                                randShot = randomShootAtOpponent();
//                            } else {
//                                randShot = randomContinueShooting(lastShot);
//                            }

                            String preparedShoot = JHandler.shotConvert(new JShoot(randShot));
                            outputStream.writeBytes(preparedShoot + "\n");
                            String testt = bufferedReader.readLine();
                            System.out.println(testt);
                            jShootResult = JHandler.shotResultConvert(testt);
                            System.out.println("my shot result: " + jShootResult.getBody());

                            switch (jShootResult.getBody()) {
                                case "HIT":
                                    opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.HIT);
                                    lastShot = randShot;
                                    disableInpossibleLocations(lastShot);
                                    break;
                                case "MISS":
                                    opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.MISS);
                                    break;
                                case "SINKING":
                                    opponentBoard.boardFields[Board.parseRow(randShot)][Board.parseColumn(randShot)].setStatus(FieldStatus.HIT);
                                    lastShot = randShot;
                                    disableInpossibleLocations(lastShot);
                                    Ship ship = opponentBoard.getShipFromBoard(lastShot);
                                    disableImpossibleLocationsAfterSinking(ship);
                                    ship.setStatus(ShipStatus.DESTROYED);
                                    for (String key : ship.partStatuses.keySet()) {
                                        ship.partStatuses.replace(key, FieldStatus.HIT);
                                    }
                                    opponentBoard.shipList.add(ship);
                                    for (Ship ship1 : opponentBoard.shipList) {
                                        System.out.println(ship1);
                                    }
                                    if (opponentBoard.checkWinConditions()) {
                                        iWon = true;
                                    }
                                    lastShot = "";
                                    break;
                                default:
                                    break;

                            }

                            if(iWon){
                                System.out.println("I WON!!!!");
                                step=5;
                                break;
                            }
                            if(iWon){
                                System.out.println("I WON!!!!");
                                step=5;
                                break;
                            }


                        }
                        if (!iWon) {
                            step++;
                        }
                        break;
                    case 3:
                        boolean iLost = false;
                        JResult jResult = new JResult("HIT");
                        JShotRequest jShotRequest = null;
                        JShotRequestResult jShotRequestResult = null;
                        while (jResult.getBody().equals("HIT") || jResult.getBody().equals("SINKING")) {
                            if (getMyBoard().checkWinConditions()) {
                                System.out.println("i lost but wait for input");
                                System.out.println("resp: "+bufferedReader.readLine());
                                iLost = true;
                                step = 6;
                                break;
                            }
                            outputStream.writeBytes(JHandler.shotRequestConvert(new JShotRequest()) + "\n");
                            jShotRequestResult = JHandler.shotRequestResultConvert(bufferedReader.readLine());
                            System.out.println("server is shooting at: " + jShotRequestResult.getBody().getRow() + jShotRequestResult.getBody().getColumn());
                            jResult.setBody(getMyBoard().shootAtField(jShotRequestResult.getBody().getRow() + jShotRequestResult.getBody().getColumn()).toString());
                            System.out.println("result: " + jResult.getBody());
                            outputStream.writeBytes(JHandler.resultConvert(jResult) + "\n");
                            System.out.println("resp: "+bufferedReader.readLine());
                           // System.out.println(printBothBoards());
                        }
                        if (!iLost) {
                            System.out.println("waiting for input string to start shooting");
                            // System.out.println("resp: "+bufferedReader.readLine());
                            step = 2;
                        }
                        break;
                    case 5:
                        JBoardSummary jBoardSummary = new JBoardSummary();
                        jBoardSummary.type = "BOARD";
                        for (Ship ship : getMyBoard().shipList) {
                            if (ship.getLength() == 4) {
                                jBoardSummary.body.four = ship.getStartField() + "-" + ship.getEndField();
                            }
                        }
                        for (Ship ship : getMyBoard().shipList) {
                            if (ship.getLength() == 3) {
                                jBoardSummary.body.three.add(ship.getStartField() + "-" + ship.getEndField());
                            }
                        }
                        for (Ship ship : getMyBoard().shipList) {
                            if (ship.getLength() == 2) {
                                jBoardSummary.body.two.add(ship.getStartField() + "-" + ship.getEndField());
                            }
                        }
                        for (Ship ship : getMyBoard().shipList) {
                            if (ship.getLength() == 1) {
                                jBoardSummary.body.one.add(ship.getStartField());
                            }
                        }
                        outputStream.writeBytes(JHandler.boardSummaryConvert(jBoardSummary) + "\n");
                        step = 10;
                        break;

                    case 6:
                        step = 5;
                        break;
                    case 10:
                        step = 1000;
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    }



