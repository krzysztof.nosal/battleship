package battle_ship.clients;

import battle_ship.boards.*;
import battle_ship.json.JsonHandler;
import battle_ship.json.jsonCommand;
import battle_ship.json.*;
import battle_ship.ships.*;

import java.io.*;
import java.util.*;

public class Client implements Runnable {
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;
    private Scanner scanner = null;
    private Board myBoard;

    public Client(DataInputStream inputStream, DataOutputStream outputStream, Scanner scanner) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.scanner = scanner;
        this.myBoard = new Board();


    }

    @Override
    public void run() {

        int step = 0;
        try {
            while (step != 999) {
                String msg = "";
                String response = "";

                switch (step) {
                    case 0:
                        System.out.println("type \"invite\" to start a game");
                        while (!msg.equals("invite")) {
                            msg = scanner.nextLine();
                        }
                        outputStream.writeUTF(JsonHandler.createJsonCommand(new jsonCommand("invitation")));
                        step++;

                        break;
                    case 1:

                        response = inputStream.readUTF();
                        jsonStatus jsonStatus = JsonHandler.getStatusFromJson(response);
                        if (jsonStatus.type.equals("invitation") && jsonStatus.status == 0) {
                            myBoard.addShip(new Ship(1, "A2", "A2"));
                            myBoard.addShip(new Ship(1, "C1", "C1"));
                            myBoard.addShip(new Ship(1, "B10", "B10"));
                            myBoard.addShip(new Ship(1, "E1", "E1"));
                            myBoard.addShip(new Ship(2, "A4", "B4"));
                            myBoard.addShip(new Ship(2, "B6", "C6"));
                            myBoard.addShip(new Ship(2, "G1", "H1"));
                            myBoard.addShip(new Ship(3, "F4", "F6"));
                            myBoard.addShip(new Ship(3, "G8", "I8"));
                            myBoard.addShip(new Ship(4, "J1", "J4"));
                            System.out.println(myBoard);
                            System.out.println("invitation accepted");
                            step++;
                        }

                        break;
                    case 2:
                        JShootResult jShootResult = new JShootResult("HIT");
                        while (jShootResult.getBody().equals("HIT") || jShootResult.getBody().equals("SINK")) {
                            System.out.println("shooot!");
                            String shooot = scanner.nextLine().toUpperCase();
                            if (Board.checkFieldSyntax(shooot)) {
                                String preparedShoot = JHandler.shotConvert(new JShoot(shooot));
                                outputStream.writeUTF(preparedShoot);
                                jShootResult = JHandler.shotResultConvert(inputStream.readUTF());
                                System.out.println("my shot result: " + jShootResult.getBody());
                            } else {
                                System.out.println("wrong field definition - try again");
                            }

                        }
                        step++;
                        break;
                    case 3:
                        JResult jResult = new JResult("HIT");
                        JShotRequest jShotRequest = null;
                        JShotRequestResult jShotRequestResult = null;
                        while (jResult.getBody().equals("HIT") || jResult.getBody().equals("SINK")) {
                            outputStream.writeUTF(JHandler.shotRequestConvert(new JShotRequest()));
                            jShotRequestResult = JHandler.shotRequestResultConvert(inputStream.readUTF());
                            System.out.println("server is shooting at: " + jShotRequestResult.getBody().getRow() + jShotRequestResult.getBody().getColumn());
                            jResult.setBody(myBoard.shootAtField(jShotRequestResult.getBody().getRow() + jShotRequestResult.getBody().getColumn()).toString());
                            System.out.println("result: " + jResult.getBody());
                            outputStream.writeUTF(JHandler.resultConvert(jResult));
                        }
                        step = 2;
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}



