package battle_ship.clients;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ClientConnectionHandler {
    private int portNumber;
    private String ipAddress;
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;

    Socket s = null;

    public ClientConnectionHandler(int portNumber, String ipAddress) {
        this.portNumber = portNumber;
        this.ipAddress = ipAddress;
    }

    public boolean connect() {
        InetAddress ip = null;
        try {
            ip = InetAddress.getByName(ipAddress);
            s = new Socket(ip, portNumber);
            inputStream = new DataInputStream(s.getInputStream());
            outputStream = new DataOutputStream(s.getOutputStream());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }
}
