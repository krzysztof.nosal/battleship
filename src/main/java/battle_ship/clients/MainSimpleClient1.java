package battle_ship.clients;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MainSimpleClient1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter ip address:");
        String ipAdress = scanner.nextLine();
        System.out.println("enter port:");
        int tcpPort = scanner.nextInt();
        ClientConnectionHandler connectionHandler = new ClientConnectionHandler(tcpPort, ipAdress);
        while (!connectionHandler.connect()) {
            System.out.println("trying to connect");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
            }

        }

    SimpleClient client = new SimpleClient("default", connectionHandler.getInputStream(), connectionHandler.getOutputStream(), scanner);
        try{
        client.run();}catch (Exception e){
            System.out.println("something went wrong");
            e.printStackTrace();}
    }
}
