package battle_ship.json;


public class JShootResult {
    private String type;
    private Integer status;
    private String message;
    private String body;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public JShootResult(String result) {
        this.type = "SHOT";
        this.status = 0;
        this.message = null;
        this.body = result;
    }


}
