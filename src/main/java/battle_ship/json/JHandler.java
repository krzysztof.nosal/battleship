package battle_ship.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;


public class JHandler {
    private JHandler() {
    }

    public static String getMessageType(String jsonString){
        Gson gson = new Gson();
        JsonObject jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();
      // jsonObject.has();
        jsonObject.isJsonObject();
        jsonObject.isJsonNull();
        jsonObject.isJsonPrimitive();

        System.out.println(jsonObject.toString());
        return jsonObject.get("type").getAsString();
    }

    public static String shotConvert(JShoot jShoot){
        Gson gson = new Gson();
        return gson.toJson(jShoot);
    }

    public static JShoot shotConvert(String json){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JShoot>() {
        }.getType();
        return gson.fromJson(json,commandType);
    }

    public static String shotResultConvert(JShootResult jShootResult){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        return gson.toJson(jShootResult);
    }
    public static JShootResult shotResultConvert(String  jShootResult){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JShootResult>() {
        }.getType();
        return gson.fromJson(jShootResult,commandType);
    }

    public static JShotRequest shotRequestConvert(String json){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JShotRequest>() {
        }.getType();
        return gson.fromJson(json,commandType);
    }

    public static String shotRequestConvert(JShotRequest json){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        return gson.toJson(json);
    }

    public static JShotRequestResult shotRequestResultConvert(String json){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JShotRequestResult>() {
        }.getType();
        return gson.fromJson(json,commandType);
    }

    public static String shotRequestResultConvert(JShotRequestResult json){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        return gson.toJson(json);
    }


    public static JResult resultConvert(String json){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JResult>() {
        }.getType();
        return gson.fromJson(json,commandType);
    }
    public static String resultConvert(JResult json){
        Gson gson = new Gson();

        return gson.toJson(json);
    }

    public static JBoardSummary boardSummaryConvert(String json){
        Gson gson = new Gson();
        Type commandType = new TypeToken<JBoardSummary>() {
        }.getType();
        return gson.fromJson(json,commandType);
    }
    public static String boardSummaryConvert(JBoardSummary json){
        Gson gson = new Gson();

        return gson.toJson(json);
    }


}

