package battle_ship.json;

public class JBoardField {
    private String row;
    private Integer column;

    public JBoardField() {
    }

    public JBoardField(String row, Integer column) {
        this.row = row;
        this.column = column;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }
}
