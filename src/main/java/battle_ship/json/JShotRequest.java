package battle_ship.json;

public class JShotRequest {
    private String type;
    private String body=null;

    public JShotRequest() {
        this.type = "SHOT_REQUEST";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
