package battle_ship.json;

import java.util.ArrayList;
import java.util.List;

public class JBoard {
    public String four;
    public List<String>three;
    public List<String>two;
    public List<String>one;

    public JBoard() {
        this.two=new ArrayList<>();
        this.three=new ArrayList<>();
        this.one=new ArrayList<>();
    }

    @Override
    public String toString() {
        return "JBoard{" +
                "four='" + four + '\'' +
                ", three=" + three +
                ", two=" + two +
                ", one=" + one +
                '}';
    }
}
