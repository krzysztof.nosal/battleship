package battle_ship.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class jsonCommand extends jsonStatement {
    public jsonCommand(String type) {
        super(type);
    }

    public jsonCommand() {
    }

    @Override
    public jsonField getBody() {
        String bodyString = String.valueOf(super.getBody());
        System.out.println("bs"+bodyString);
        Gson gson = new Gson();
        Type fieldType = new TypeToken<jsonField>() {
        }.getType();
        return gson.fromJson(bodyString,fieldType);
    }


}
