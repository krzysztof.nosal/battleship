package battle_ship.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class JsonHandler {
    private JsonHandler() {
    }

    public static jsonStatement getStatementFromJson(String json){
        Gson gson = new Gson();
        jsonStatement jsonCommand;
        Type commandType = new TypeToken<jsonStatement>() {
        }.getType();
        jsonCommand=gson.fromJson(json,commandType);
        return jsonCommand;
    }
    public static jsonField getJsonFieldFromBody(Object body){
        Gson gson = new Gson();
        Type fieldType = new TypeToken<jsonField>() {
        }.getType();
        return gson.fromJson(String.valueOf(body),fieldType);

    }
    public static jsonCommand getCommandFromJson(String json){
        Gson gson = new Gson();
        jsonCommand jsonCommand;
        Type commandType = new TypeToken<jsonCommand>() {
        }.getType();
        jsonCommand=gson.fromJson(json,commandType);
        return jsonCommand;
    }
    public static jsonStatus getStatusFromJson(String json){
        Gson gson = new Gson();
        jsonStatus jsonStatus;
        Type commandType = new TypeToken<jsonStatus>() {
        }.getType();
        jsonStatus = gson.fromJson(json,commandType);
        return jsonStatus;
    }

    public static jsonResult getResultFromJson(String json){
        Gson gson = new Gson();
        jsonResult jsonResult;
        Type commandType = new TypeToken<jsonResult>() {
        }.getType();
        jsonResult=gson.fromJson(json,commandType);
        return jsonResult;
    }
    public static String createJsonResponse(jsonCommand jsonCommand){
        Gson gson = new Gson();
        return gson.toJson(jsonCommand);
    }

    public static String createJsonResponse(jsonResult jsonResult){
        Gson gson = new Gson();
        return gson.toJson(jsonResult);
    }

    public static String createJsonCommand(jsonCommand jsonCommand){

        return createJsonResponse(jsonCommand);
    }
}
