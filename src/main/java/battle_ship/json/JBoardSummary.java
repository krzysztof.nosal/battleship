package battle_ship.json;

public class JBoardSummary {
    public String type;
    public JBoard body;

    public JBoardSummary() {
        this.body = new JBoard();
    }

    @Override
    public String toString() {
        return "JBoardSummary{" +
                "type='" + type + '\'' +
                ", body=" + body +
                '}';
    }
}
