package battle_ship.json;

import battle_ship.boards.Board;

public class JShotRequestResult {
    private String type;
    private Integer status;
    private String message;
    private JBoardField body;



    public JShotRequestResult(String field){
        this.type="SHOT_REQUEST";
        this.status=0;
        this.message=null;
        this.body=new JBoardField();
        this.body.setRow(Board.rowToString(Board.parseRow(field)));
        this.body.setColumn(Board.parseColumn(field)+1);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JBoardField getBody() {
        return body;
    }

    public void setBody(JBoardField body) {
        this.body = body;
    }



}
