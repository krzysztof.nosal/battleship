package battle_ship.json;

public class JResult {
    private String type;
    private String body;

    public JResult() {
    }

    public JResult(String body) {
        this.type = "RESULT";
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
