package battle_ship.json;

import battle_ship.boards.Board;

public class JShoot {
    private String type;
    private JBoardField body;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JShoot(String field){
        this.type="SHOT";
        this.body = new JBoardField();
        this.body.setRow(Board.rowToString(Board.parseRow(field)));
        this.body.setColumn(Board.parseColumn(field)+1);
    }
    public String getFieldString(){
        return this.body.getRow()+this.body.getColumn();
    }

}
