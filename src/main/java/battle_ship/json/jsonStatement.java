package battle_ship.json;

public class jsonStatement {
    private String type;
    private int status;
    private String message;
    private Object body;

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getBody() {
        return body;
    }

    public jsonStatement() {
    }

    public jsonStatement(String type) {
        this.type = type;
    }
}
