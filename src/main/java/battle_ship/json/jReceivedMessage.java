package battle_ship.json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

public class jReceivedMessage {
    private String type;
    private int status;
    private String message;
    private String body;
    private JBoardField jBoardField;

    public jReceivedMessage(String type) {
        this.type = type;
        this.jBoardField=new JBoardField();
    }

    public int jsonImport(String jsonString){
        JsonObject jsonObject=null;
        try{
        jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();}
        catch (Exception e){
            return 1;
        }
        if(jsonObject==null){
            return 1;
        }
        if(jsonObject.has("type")){
            if(!jsonObject.get("type").getAsString().equals(this.type)){
               return 3;
            }
        }else{
            return 2;
        }
        if(jsonObject.has("status")){
            this.status = jsonObject.get("status").getAsInt();
        }
        if(jsonObject.has("message")){
            if(jsonObject.get("message").isJsonNull()){
                this.message=null;
            }else{
            this.message = jsonObject.get("message").getAsString();}
        }

        if(jsonObject.has("body")){
            if(jsonObject.get("body").isJsonNull()){
                this.body=null;
            }else{
                if(jsonObject.get("body").isJsonObject()){
                    JsonObject jsonField = jsonObject.get("body").getAsJsonObject();
                    if(jsonField.has("row")&&jsonField.has("column")){
                        jBoardField.setRow(jsonField.get("row").getAsString());
                        jBoardField.setColumn(jsonField.get("column").getAsInt());
                    }else{
                        return 2;
                    }
                }else{
                this.body = jsonObject.get("body").getAsString();}}
        }

        return 0;

    }

    public String jsonExport(String messageType, String author){
        JsonObject jsonObject = new JsonObject();
        if(author.equals("server")){
            switch (messageType){
                case "GAME_INVITATION":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("status",this.status);
                    jsonObject.addProperty("message",this.message);
                    jsonObject.add("body",null);
                    return jsonObject.toString();

                case "SHOT":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("status",this.status);
                    jsonObject.addProperty("message",this.message);
                    jsonObject.addProperty("body", this.body);
                    return jsonObject.toString();

                case "SHOT_REQUEST":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("status",this.status);
                    jsonObject.addProperty("message",this.message);
                    JsonObject jsonField = new JsonObject();
                    jsonField.addProperty("row",this.jBoardField.getRow());
                    jsonField.addProperty("column",this.jBoardField.getColumn());
                    jsonObject.add("body", jsonField);
                    return jsonObject.toString();

                case "RESULT":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("status",0);
                    jsonObject.add("message",null);
                    jsonObject.add("body",null);
                    return jsonObject.toString();
                case "UNKNOWN":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("status",4);
                    jsonObject.addProperty("message",this.message);
                    jsonObject.addProperty("body", this.body);
                    return jsonObject.toString();

            }
        }
        if(author.equals("client")){
            switch (messageType){
                case "GAME_INVITATION":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.add("body",null);
                    return jsonObject.toString();

                case "SHOT":
                    jsonObject.addProperty("type",this.type);
                    JsonObject jsonField = new JsonObject();
                    jsonField.addProperty("row",this.jBoardField.getRow());
                    jsonField.addProperty("column",this.jBoardField.getColumn());
                    jsonObject.add("body", jsonField);
                    return jsonObject.toString();

                case "SHOT_REQUEST":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.add("body",null);
                    return jsonObject.toString();

                case "RESULT":
                    jsonObject.addProperty("type",this.type);
                    jsonObject.addProperty("body",this.body);
                    return jsonObject.toString();

            }
        }
        return null;
    }

    public String getFieldAsString(){
        if(jBoardField.getRow()!=null){
        return this.jBoardField.getRow()+jBoardField.getColumn();}
        else {return null;}
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public JBoardField getjBoardField() {
        return jBoardField;
    }

    public void setjBoardField(JBoardField jBoardField) {
        this.jBoardField = jBoardField;
    }
}
