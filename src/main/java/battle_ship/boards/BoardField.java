package battle_ship.boards;

public class BoardField {
    private FieldStatus status;

    public BoardField() {
        status=FieldStatus.EMPTY;
    }

    public FieldStatus getStatus() {
        return status;
    }

    public void setStatus(FieldStatus status) {
        this.status = status;
    }
}
