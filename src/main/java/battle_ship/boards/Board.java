package battle_ship.boards;

import battle_ship.players.PlayerActionResult;
import battle_ship.ships.Ship;
import battle_ship.ships.ShipStatus;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Board {
    public BoardField[][] boardFields;
    public List<Ship> shipList;
    private static final Logger LOGGER = Logger.getLogger(Board.class.getName());
    ConsoleHandler handler = new ConsoleHandler();
    SimpleFormatter formatter = new SimpleFormatter();

    public Board() {
        LOGGER.addHandler(handler);
        handler.setFormatter(formatter);
        handler.setLevel(Level.ALL);
        this.boardFields = new BoardField[10][10];
        shipList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.boardFields[i][j] = new BoardField();
            }
        }
    }

    public boolean canAddShip(Ship ship) {
        int startRow = parseRow(ship.getStartField());
        int endRow = parseRow(ship.getEndField());
        int startCol = parseColumn(ship.getStartField());
        int endCol = parseColumn(ship.getEndField());
        if (boardFields[startRow][startCol].getStatus() != FieldStatus.EMPTY) {
            return false;
        }
        if (!Ship.checkSyntax(ship.getStartField() + ":" + ship.getEndField())) {
            return false;
        }
        if (startRow == endRow) {
            if (startCol > 0 && ((startRow > 0 && boardFields[startRow - 1][startCol - 1].getStatus() != FieldStatus.EMPTY) || (startRow < 9 && boardFields[startRow + 1][startCol - 1].getStatus() != FieldStatus.EMPTY) || (boardFields[startRow][startCol - 1].getStatus() != FieldStatus.EMPTY))) {
                return false;
            }
            int lastColumn = 0;
            for (int i = startCol; i <= endCol; i++) {
                if (startRow > 0) {
                    if (boardFields[startRow - 1][i].getStatus() != FieldStatus.EMPTY) {
                        return false;
                    }

                    if (i < 9 && boardFields[startRow - 1][i + 1].getStatus() != FieldStatus.EMPTY) {
                        return false;
                    }

                }
                if (startRow < 9) {
                    if (boardFields[startRow + 1][i].getStatus() != FieldStatus.EMPTY) {
                        return false;
                    }
                    if (i < 9 && boardFields[startRow + 1][i + 1].getStatus() != FieldStatus.EMPTY) {
                        return false;
                    }
                }

                lastColumn = i;
                if (lastColumn < 9 && boardFields[startRow][lastColumn + 1].getStatus() != FieldStatus.EMPTY) {
                    return false;
                }
            }

        } else {
            if (startRow > 0) {
                if (startCol > 0 && (boardFields[startRow - 1][startCol - 1].getStatus() != FieldStatus.EMPTY)) {
                    return false;
                }
                if (startCol < 9 && (boardFields[startRow - 1][startCol + 1].getStatus() != FieldStatus.EMPTY)) {
                    return false;
                }
                if (boardFields[startRow - 1][startCol].getStatus() != FieldStatus.EMPTY) {
                    return false;
                }
            }
            int lastRow = 0;
            for (int i = startRow; i <= endRow; i++) {
                if (startCol > 0){
                if ( (boardFields[i][parseColumn(ship.getStartField()) - 1].getStatus() != FieldStatus.EMPTY || (i < 9 && boardFields[i + 1][parseColumn(ship.getStartField()) - 1].getStatus() != FieldStatus.EMPTY))) {
                    return false;
                }}
                if(startCol<9){
                if (parseRow(ship.getStartField()) < 9 && (boardFields[i][parseColumn(ship.getStartField()) + 1].getStatus() != FieldStatus.EMPTY || (i < 9 && boardFields[i + 1][parseColumn(ship.getStartField()) + 1].getStatus() != FieldStatus.EMPTY))) {
                    return false;
                }}
                lastRow = i;
                if(lastRow < 9){
                if ( boardFields[lastRow + 1][parseColumn(ship.getStartField())].getStatus() != FieldStatus.EMPTY) {
                    return false;
                }}


            }


        }

        return true;

    }

    public int addShip(Ship ship) {
        if (!canAddShip(ship)) {
            System.out.println("cannot add ship: ");
            //ship.printShip();
            return 1;
        }

        if (parseRow(ship.getStartField()) == parseRow(ship.getEndField())) {

            for (int i = parseColumn(ship.getStartField()); i <= parseColumn(ship.getEndField()); i++) {

                this.boardFields[parseRow(ship.getStartField())][i].setStatus(FieldStatus.FULL);
            }
        } else {
            for (int i = parseRow(ship.getStartField()); i <= parseRow(ship.getEndField()); i++) {
                this.boardFields[i][parseColumn(ship.getStartField())].setStatus(FieldStatus.FULL);
            }

        }
        shipList.add(ship);
        return 0;
    }

    public static int parseRow(String field) {
        int row = 0;
        row = field.charAt(0) - 65;
        return row;
    }

    public static int parseColumn(String field) {
        int column = 0;
        column = Integer.valueOf(field.substring(1)) - 1;
        return column;
    }

    public static String rowToString(int row) {
        return Character.toString((char) row + 65);
    }

    public boolean checkWinConditions() {
        int oneMastSinken = 0;
        int twoMastSinken = 0;
        int threeMastSinken = 0;
        int fourMastSinken = 0;
        for (Ship ship : shipList) {
            if (ship.getLength() == 1 && ship.getStatus().equals(ShipStatus.DESTROYED)) {
                oneMastSinken++;
            }
            if (ship.getLength() == 2 && ship.getStatus().equals(ShipStatus.DESTROYED)) {
                twoMastSinken++;
            }
            if (ship.getLength() == 3 && ship.getStatus().equals(ShipStatus.DESTROYED)) {
                threeMastSinken++;
            }
            if (ship.getLength() == 4 && ship.getStatus().equals(ShipStatus.DESTROYED)) {
                fourMastSinken++;
            }
        }
        return oneMastSinken == 4 && threeMastSinken == 2 && twoMastSinken == 3 && fourMastSinken == 1;
    }

    public FieldStatus getStatusOfField(String field) {
        return this.boardFields[parseRow(field)][parseColumn(field)].getStatus();
    }

    public static boolean checkFieldSyntax(String field) {
        Pattern pattern = Pattern.compile("[A-J][1-9][0]?");
        Matcher matcher = pattern.matcher(field);
        boolean matches = matcher.matches();
        if (!matches) {
            LOGGER.log(Level.SEVERE, "regex mismatch");
            return false;
        }

        if (Integer.parseInt(field.substring(1)) > 10) {
            LOGGER.log(Level.SEVERE, "to big col number");
            return false;

        }
        return true;
    }

    public PlayerActionResult shootAtField(String field) {
        if (this.boardFields[parseRow(field)][parseColumn(field)].getStatus() == FieldStatus.FULL) {
            this.boardFields[parseRow(field)][parseColumn(field)].setStatus(FieldStatus.HIT);
            for (Ship ship : shipList) {
                if (ship.getStartField().equals(getShipFromBoard(field).getStartField())) {
                    ship.partStatuses.replace(field, FieldStatus.HIT);
                    ship.evaluateStatus();
                    if (ship.getStatus() == ShipStatus.DESTROYED) {
                        return PlayerActionResult.SINKING;
                    } else {
                        return PlayerActionResult.HIT;
                    }

                }
            }
            return PlayerActionResult.ERROR;
        } else {
            this.boardFields[parseRow(field)][parseColumn(field)].setStatus(FieldStatus.MISS);
            return PlayerActionResult.MISS;
        }
    }

    public static String fieldStatusShortcut(FieldStatus status) {
        switch (status) {
            case EMPTY:
                return ",";
            case FULL:
                return "0";
            case HIT:
                return "X";
            case MISS:
                return "-";
            default:
                return ",";

        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("/////////////////////\n");
        stringBuilder.append("|~ 1 2 3 4 5 6 7 8 9 10|\n");
        for (int i = 0; i < 10; i++) {
            stringBuilder.append("|").append(rowToString(i)).append(" ");
            for (int j = 0; j < 10; j++) {
                stringBuilder.append(fieldStatusShortcut(boardFields[i][j].getStatus())).append(" ");
            }
            stringBuilder.append("|\n");
        }
        return stringBuilder.toString();
    }


    public Ship getShipFromBoard(String field) {
        int row = parseRow(field);
        int column = parseColumn(field);
        String startField = field;
        String endField = field;
        boolean isVertical = false;
        int length = 1;
        if (row > 0) {
            while (row > 0) {
                if (boardFields[row - 1][column].getStatus().equals(FieldStatus.FULL) || boardFields[row - 1][column].getStatus().equals(FieldStatus.HIT)) {
                    row--;
                    startField = rowToString(row) + (column + 1);
                    isVertical = true;
                    length++;
                } else {
                    break;
                }
            }
            row = parseRow(field);
        }
        while (row < 9) {
            if (boardFields[row + 1][column].getStatus().equals(FieldStatus.FULL) || boardFields[row + 1][column].getStatus().equals(FieldStatus.HIT)) {
                row++;
                endField = rowToString(row) + (column + 1);
                isVertical = true;

                length++;
            } else {
                break;
            }
        }
        if (!isVertical) {
            while (column > 0) {
                if (boardFields[row][column - 1].getStatus() == FieldStatus.FULL || boardFields[row][column - 1].getStatus() == FieldStatus.HIT) {
                    column--;
                    startField = rowToString(row) + (column + 1);

                    length++;
                } else {
                    break;
                }
            }
            column = parseColumn(field);
            while (column < 9) {
                if (boardFields[row][column + 1].getStatus() == FieldStatus.FULL || boardFields[row][column + 1].getStatus() == FieldStatus.HIT) {
                    column++;
                    endField = rowToString(row) + (column + 1);
                    length++;
                } else {
                    break;
                }
            }
        }
        return new Ship(length, startField, endField);
    }
}
