package battle_ship.boards;

public enum FieldStatus {
    EMPTY, FULL, HIT, MISS, EXCLUDED;
}
