package battle_ship;

import battle_ship.boards.Board;
import battle_ship.json.JBoardField;
import battle_ship.json.jReceivedMessage;
import battle_ship.ships.Ship;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class BoardTest {


        @Test
        @DisplayName("")
    public void incorrectJsonParsing(){
            jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
            int result = jReceivedMessage.jsonImport("{\"dupa\":0\n");
            Assert.assertEquals(1,result);
        }

    @Test
    @DisplayName("")
    public void wrongMessageType(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT_REQUEST\"}\n");
        Assert.assertEquals(3,result);
    }

    @Test
    @DisplayName("")
    public void wrongMessageStructure(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"typ\":\"SHOT_REQUEST\"}\n");
        Assert.assertEquals(2,result);
    }
    @Test
    @DisplayName("")
    public void nullMessageInterpretation(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":null}\n");
        Assert.assertEquals(0,result);
        Assert.assertNull(jReceivedMessage.getMessage());
    }

    @Test
    @DisplayName("")
    public void notNullMessageInterpretation(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\"}\n");
        Assert.assertEquals(0,result);
        Assert.assertEquals("test message",jReceivedMessage.getMessage());
    }

    @Test
    @DisplayName("")
    public void nullBodyInterpretation(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\",\"body\":null}\n");
        Assert.assertEquals(0,result);
        Assert.assertNull(jReceivedMessage.getBody());
    }

    @Test
    @DisplayName("")
    public void stringBodyInterpretation(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\",\"body\":\"test\"}\n");
        Assert.assertEquals(0,result);
        Assert.assertEquals("test",jReceivedMessage.getBody());
    }

    @Test
    @DisplayName("")
    public void fieldBodyInterpretation(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\",\"body\":{\"row\":\"B\",\"column\":\"2\"}}\n");
        Assert.assertEquals(0,result);
        Assert.assertEquals("B",jReceivedMessage.getjBoardField().getRow());
        Assert.assertEquals(2,jReceivedMessage.getjBoardField().getColumn());
    }

    @Test
    @DisplayName("")
    public void gameInvitationExport(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("GAME_INVITATION");
        //int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\",\"body\":{\"row\":\"B\",\"column\":\"2\"}}\n");
        //Assert.assertEquals(0,result);
        Assert.assertEquals("{\"type\":\"GAME_INVITATION\",\"body\":null}",jReceivedMessage.jsonExport("GAME_INVITATION","client"));
       // Assert.assertEquals(2,jReceivedMessage.getjBoardField().getColumn());
    }

    @Test
    @DisplayName("")
    public void shotOfClient(){
        jReceivedMessage jReceivedMessage = new jReceivedMessage("SHOT");
        //int result = jReceivedMessage.jsonImport("{\"type\":\"SHOT\",\"message\":\"test message\",\"body\":{\"row\":\"B\",\"column\":\"2\"}}\n");
        //Assert.assertEquals(0,result);
        jReceivedMessage.setjBoardField(new JBoardField("A",3));
        Assert.assertEquals("{\"type\":\"SHOT\",\"body\":{\"row\":\"A\",\"column\":3}}",jReceivedMessage.jsonExport("SHOT","client"));
        // Assert.assertEquals(2,jReceivedMessage.getjBoardField().getColumn());
    }
}
